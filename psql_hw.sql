-- Start Home WORK
-- Напишите sql-команду для создания таблицы workers, как на скриншоте выше;
create table workers(
id SERIAL PRIMARY KEY,
name varchar (50) not null,
age integer not null,
salary integer
);
-- После создания таблицы, добавьте к ней колонку last_login, типа int, которая не может быть пустой;
alter table workers add column last_login int not null;
-- Добавьте еще одну колонку test произвольного типа и удалите ее;
alter table workers add column test VARCHAR(15);
alter table workers drop column test;
-- Переименуйте колонку last_login в last_login_time;
alter table workers rename column last_login to last_login_time;
-- Изменить дефолтное значение last_login_time на 0;
ALTER TABLE workers ALTER COLUMN last_login_time SET DEFAULT 0;
-- Измените колонку last_login_time, чтобы она теперь могла быть пустой;
ALTER TABLE workers ALTER COLUMN last_login_time DROP NOT NULL;
-- Заполните таблицу workers данными (в один запрос), как на скриншоте, можете добавить дополнительных данных.
INSERT INTO workers (name, age, salary)
		VALUES ('Дима', 20, 2000),
           ('Петя', 25, 3000),
           ('Вася', 35, 4000),
           ('Коля', 40, 5000),
           ('Никита', 50, 6000),
           ('Иван', 23, 1000),
           ('Оля', 18, 5000),
           ('Карина', 18, 1000),
           ('Вероника', 23, 3000);
-- Измените поля с id 2 и 4, вставьте в них текущее Unix-время;
--alter table workers add column wtime VARCHAR;

--update workers set wtime = NOW() where id = 2 and id = 4;

-- Создайте 2 комментария к любым (выбранным вами) полям.
-- select * from workers where salary = 555;
-- select * from workers where id = 5;

-- Задача 2: Выбрать работника с id = 3
select * from workers where id = 3;
-- Задача 3: Выбрать работников с зарплатой 1000$.
select * from workers where salary = 1000;
-- Задача 4: Выбрать работников в возрасте 23 года.
select * from workers where age = 23;
-- Задача 5: Выбрать работников с зарплатой более 400$.
select * from workers where salary > 400;
-- Задача 6: Выбрать работников с зарплатой равной или большей 500$.
select * from workers where salary >= 500;
-- Задача 7: Выбрать работников с зарплатой НЕ равной 500$.
select * from workers where salary != 500;
-- Задача 8: Выбрать работников с зарплатой равной или меньшей 900$.
select * from workers where salary <= 900;
-- Задача 9: Узнайте зарплату и возраст Васи.
select * from workers where name = 'Вася';
-- Задача 10: Выбрать работников в возрасте от 25 (не включительно) до 28 лет (включительно).
select * from workers where age >= 25 and age <=28;
-- Задача 11: Выбрать работника Петю.
select * from workers where name = 'Петя';
-- Задача 12: Выбрать работников Петю и Васю.
select * from workers where name = 'Петя' and name = 'Вася';
-- Задача 13: Выбрать всех, кроме работника Петя.
select * from workers where name != 'Петя';
-- Задача 14: Выбрать всех работников в возрасте 27 лет или с зарплатой 1000$.
select * from workers where age = 27 or salary = 1000;
-- Задача 15: Выбрать всех работников в возрасте от 23 лет (включительно) до 27 лет (не включительно) или с зарплатой 1000$
select * from workers where (age >= 23 and age < 27) or salary = 1000;
-- Задача 16: Выбрать всех работников в возрасте от 23 лет до 27 лет или с зарплатой от 400$ до 1000$.
select * from workers where (age > 23 and age < 27) or (salary>400 and < 1000);
-- Задача 17: Выбрать всех работников в возрасте 27 лет или с зарплатой не равной 400$.
select * from workers where age = 27 or salary != 400;
-- Задача 18 (на UPDATE, 27 строка в файле с командами вам подскажет):
-- Поставьте Васе зарплату в 200$.
update workers set salary = 200 where name='Вася';
-- Работнику с id=4 поставьте возраст 35 лет.
update workers set age = 35 where id = 4;

-- Измените поля с id 2 и 4, вставьте в них текущее Unix-время;
update workers set workers.time = NOW() where id = 2 and id = 4;

-- Задача 19: (на DELETE, 28 строка в файле с командами вам подскажет):
-- Удалите Колю.
delete from workers where name = 'Коля';
-- Удалите всех работников, у которых возраст 23 года.
delete from workers where age = 23;

